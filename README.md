Please don't use this. Instead, run: `localectl set-x11-keymap us chromebook`
on a modern release. [1](https://wiki.archlinux.org/index.php/Chrome_OS_devices#Hotkeys)

To use this file (Please don't):

    git clone https://gitlab.com/onii/c720-udev-hwdb.git
    cd c720-udev-hwdb
    sudo cp 61-keyboard.hwdb /etc/udev/hwdb.d/
    sudo udevadm hwdb --update

And reboot. Now pressing the keys should do stuff.
